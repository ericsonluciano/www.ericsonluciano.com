var Metalsmith = require('metalsmith')
var drafts = require('metalsmith-drafts');
var markdown = require('metalsmith-markdown');
var collections = require('metalsmith-collections');
var permalinks = require('metalsmith-permalinks');

var app = Metalsmith(__dirname)
  .source('./web/source')
  .destination('./public')
  .clean(true)
  .use(drafts())
  .use(collections({
    posts: {
        pattern: 'posts/*.md',
        sortBy: 'date',
        reverse: true
    },
    works: {
        pattern: 'works/*.md',
        sortBy: 'date',
        reverse: true
    },
    pages: {
        pattern: '*.md',
    }
  }))
  .use(markdown())
  .use(permalinks())
  .use(require('metalsmith-jstransformer')({
    'pattern': '**',
    'layoutPattern': 'layouts/**',
    'defaultLayout': null
  }))
  .use(require('metalsmith-sense-sass')())
  .use(require('metalsmith-browserify-alt')({
    defaults: {
      cache: {},
      packageCache: {},
      transform: ['babelify'],
      plugin: process.env.NODE_ENV === 'development' ? ['watchify'] : []
    }
  }))

if (process.env.NODE_ENV === 'production') {
  app = app.use(require('metalsmith-uglifyjs')({
    override: true,
    uglifyOptions: {
      mangle: true, compress: { warnings: false }
    }
  }))
}

if (module.parent) {
  module.exports = app
} else {
  app.build(function (err) { if (err) { console.error(err); process.exit(1) } })
}
